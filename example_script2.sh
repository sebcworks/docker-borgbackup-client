#!/bin/bash

# Alternative script where you can specify what to do through command line.
#
# Arguments are:
# 'backup directory [exclude-file-name]'
# 'prune prefix [prune-strategy]'
#
# example:
# If I have inside $DATADIR :
# > ls -1 $DATADIR
# toto
# titi
# tata
#
# Then, I can do
# docker run --name borg-client-daily-titi -v borgbackup-scripts:/home/borg/scripts -v borgbackup-data-ssh:/home/borg/.ssh -v /path/to/data:/home/borg/data:ro sebcworks/borgbackup-client daily backup titi exclude_specifics_titi_paths
#
# And I can add a cron to run:
# docker start -a borg-client-daily-titi
#

if [ -z "$1" -o -z "$2" ]; then
    echo "FATAL: Not enough arguments given."
    exit 1
fi

SERVER_IP=aaa.bbb.ccc.ddd
SERVER_PORT=2222
REPOSITORY_NAME=repository-name

# May be any other directory accessible to the user who run the script ("root" by default)
DATADIR=/home/borg/data

REPOS="borg@$SERVER_IP:$REPOSITORY_NAME"
BACKUP_DATE=`date "+%Y-%m-%d.%H:%M:%S"`
LOGFILE=$DATADIR/borgbackup_${BACKUP_DATE}.log

STATUS_FILE="/home/borg/scripts/.running_borg_backup"

OPTS="--one-file-system --exclude-caches -v --stats --compression zlib,4"
PRUNE_OPTS="-v -s"

# Optionnaly encrypted passphrase,
# encryption done with command: echo -n "unencrypted passphrase" | openssl enc -aes-256-cbc -a -e -k $PASSPHRASE_ENC > $PASSPHRASE_FILE
PASSPHRASE_FILE="/home/borg/scripts/key.to.the.attic"
PASSPHRASE_ENC="WeNeverKnow"

# Get the encrypted passphrase and decrypt it
if [ ! -r "$PASSPHRASE_FILE" ]; then
    echo "FATAL ERROR: Can't read the passphrase file!"
    exit
fi

PASSPHRASE=`cat $PASSPHRASE_FILE | openssl enc -aes-256-cbc -a -d -k $PASSPHRASE_ENC`

# Skip if previous wasn't finished
if [ -f "$STATUS_FILE" ]; then
    echo "FATAL: Can't run Borg as it seems to be already running"
    exit
fi

echotime () {
  echo "[`date "+%Y-%m-%d %H:%M:%S"`]: $1"
}

# Log everything
exec >> $LOGFILE 2>&1




# Export the passphrase
export BORG_PASSPHRASE=$PASSPHRASE

# Export the ssh command to run
# Note: To avoid problems with "missing TTY", we have to add the option with the path to the unknown hosts file.
# With this configuration, we have to know the server, so we have to connect once with the volume of ssh data (for example, at repository init)
# But we have to "renew" this file if we re-create the server container or if we do anything that will change its keys.
export BORG_RSH="ssh -p $SERVER_PORT -i /home/borg/.ssh/id_rsa -o UserKnownHostsFile=/home/borg/.ssh/known_hosts"




echotime "Starting Borg backup script: $0"
touch "$STATUS_FILE"

# What do we want to do?
ACTION=$1
ARG=$2
EXCLUDEFILE=/home/borg/scripts/$3

if [ "$ACTION" = "backup" ]; then
    # ARG is the sub-folder of $DATADIR to save
    if [ ! -r "$DATADIR/$ARG" ]; then
	echo "ERROR: Unable to do the backup, directory not found: $DATADIR/$ARG"
	exit 1
    fi

    # If we have an exclude file, add it to the options (remember: 1 path per line)
    # Absolute paths are under $DATADIR !!!
    if [ -r "$EXCLUDEFILE" ]; then
        OPTS="$OPTS --exclude-from $EXCLUDEFILE"
    fi

    echotime "Doing backup of $DATADIR/$ARG"
    borg create $OPTS $REPOS::$ARG-$BACKUP_DATE $DATADIR/$ARG
elif [ "$ACTION" = "prune" ]; then
    # ARG is the prefix to use to restrict the pruning strategy to some specific archives
    # We may have a 3rd argument which is the "keep" parameters.
    
    prune_keep_params="--keep-daily 3 --keep-weekly 3 --keep-monthly 3"
    if [ ! -z "$3" ]; then
	prune_keep_params="$3-"
    fi
    echotime "Prune old archives following these rules: $prune_keep_params"
    borg prune $PRUNE_OPTS $REPOS --prefix $ARG $prune_keep_params
else
    echotime "Unknown action. Doing nothing."
fi

rm "$STATUS_FILE"
echotime "Script ended."

