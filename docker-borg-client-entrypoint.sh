#!/bin/bash

set -e


# We should receive as an argument which script we will run
# 4 possibilities: hourly, daily, weekly and monthly
# Each possibility match a script that has to be executable.
#
# An example script is given, written in bash

if [ -z "$1" ]; then
    echo "ERROR: No argument given, don't know what to do."
    exit 1
fi

# Check that we are able to read the source directory
if [ ! -r "/home/borg/data" ]; then
    echo "ERROR: Unable to read the source data directory."
    exit 2
fi

sdir=/home/borg/scripts

# Check that we are able to read the script directory
if [ ! -d "$sdir" -o ! -x "$sdir" ]; then
    echo "ERROR: Unable to read the script directory."
    exit 2
fi

case $1 in
    hourly | daily | weekly | monthly ) test -x $sdir/$1 && exec $sdir/$1 ${@:2};;
    start ) echo "Borg Client didn't found any problem."
	    echo "You may launch it with hourly/daily/weekly/monthly parameter."
	    exit 0;;
    * ) echo "ERROR: Unknown argument: $1"
	exit 3;;
esac

echo "WARNING: Asked script not executable or unknown problem. Please check file rights for $sdir/$1"
