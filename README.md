Borg Client Container
=====================

Description
-----------

Borg backup client container. It uses `sebcworks/borgbackup-base` as base image and will launch a script depending on the asked argument.

It's a good idea to check how the Server may be used: `sebcworks/borgbackup-server`

See Usage for more informations.

Initial Setup
-------------

Prior to use it, you have to prepare a volume (or folder, but I will assume you'll use a volume) with the ssh key that is accepted by the server (see `sebcworks/borgbackup-server`).

**WARNING** With Docker 1.9, never launch a container with `--rm` flag if you are mounting volumes to it, otherwise they will be deleted at container exit.
This behaviour has changed from 1.10 (Docker will no longer remove "named" volumes).


    docker volume create --name borgbackup-data-ssh
    docker run -it --name borg-init-ssh-client -v borgbackup-data-ssh:/sshdata sebcworks/borgbackup-base /bin/bash
    > su borg
    > ssh-keygen
    > exit
    > cp -a /home/borg/.ssh/* /sshdata
    -> Copy the public key
    > exit
    docker rm borg-init-ssh-client

You should also init the repository from a `sebcworks/borgbackup-base` container: it allows you to check the access to the server

    docker run -it --name borg-init-repository -v borgbackup-data-ssh:/home/borg/.ssh sebcworks/borgbackup-base /bin/bash
    > su borg
    > borg init --encryption=repokey user@server-host:repository-name
    -> OR, if you use a custom ssh port (2222 in this example):
    > env BORG_RSH="ssh -p 2222" borg init --encryption=repokey user@server-host:repository-name
    > exit
    docker rm borg-init-repository

Usage
-----

This suppose that you have an initialized repository.

The idea is to have one container created per "save" to run, and then launching these containers (with a cron) to do the periodic backup.

Backup process is dependant of what is contained in the backup scripts you will use.

If you later need to update or fix a backup script, you can launch a `sebcworks/borgbackup-base` container and edit them:

    docker run -it --name borg-edit-scripts -v borgbackup-scripts:/home/borg/scripts sebcworks/borgbackup-base /bin/bash
    > apt update && apt install nano
    -> do what you need
    > exit
    docker rm borg-edit-scripts 


### Very important, to read before anything else

Please keep in mind that the scripts will be run as `root`. This is necessary to be able to save any data and not only data accessible by borg user.

In the given example scripts, put within the client, I refer to the /home/borg/data directory. You don't have to use this directory, especially if you want to mount directories from other containers (through the `--volumes-from` argument) that can't be mounted to /home/borg/data. **To avoid problem, don't forget to set the read only (`ro`) flag to the mount**.

Keep this statement in mind when you write your scripts.

### Creating the scripts

In order to have a working solution, you will have to prepare the scripts that will be used.

There is 4 different scripts that may be called at different frequency (notice that there is no extension):

1. `hourly`
2. `daily`
3. `weekly`
4. `monthly`

Of course, the content and the use of these scripts is up to you, so if you want to run the hourly script twice a day, you're free to do so.

I gave 2 example scripts in `/home/borg/` directory (files called `example_script.sh` and `example_script2.sh`) to give an example of what may reside in those scripts. You can get them from a `sebcworks/borgbakup-client` container or from the git repository: (https://bitbucket.org/sebcworks/docker-borgbackup-client/src)

Anyway, you will need either to map the folder or a volume with the scripts to `/home/borg/scripts`, these scripts has to be executable for borg user. I prefer the volume approach to avoid problems with permissions.

**IMPORTANT:** In my example scripts, I refer to the passphrase as being written in a file located in `/home/borg/scripts`, so it's during the steps of creating the scripts that you may create the file with the passphrase. You can "skip" this if the passphrase is written in clear text in your scripts (or if you're not using a passphrase).

So, if I'm working with volumes:

    docker volume create --name borgbackup-scripts
    docker run -it --name borg-create-scripts -v borgbackup-scripts:/home/borg/scripts sebcworks/borgbackup-base /bin/bash
    > apt update && apt install nano
    > nano hourly
    -> fill script...
    > chown borg.borg hourly
    > chmod 0750 hourly
    -> repeat for the 3 others if you want
    -> you may also save the passphrase in a file as in my example
    exit
    docker rm borg-create-scripts

### Create the client container(s)

The goal here is to have a client container created for every scripts that we want to run, and a cron job to start them on a regular basis.

So, let's create a container for daily and for monthly work.

In the line below, you can append as many arguments as needed after 'daily' and 'monthly' keyword depending on your scripts.

Beware that doing the run command will actually launch the scripts, so it will do the "first" backup

    docker run --name borg-client-daily -v borgbackup-scripts:/home/borg/scripts -v borgbackup-data-ssh:/home/borg/.ssh -v /path/to/data:/home/borg/data:ro sebcworks/borgbackup-client daily
    -> OR:
    docker run --name borg-client-daily -v borgbackup-scripts:/home/borg/scripts -v borgbackup-data-ssh:/home/borg/.ssh --volumes-from my-container:ro sebcworks/borgbackup-client daily

    docker run --name borg-client-monthly -v borgbackup-scripts:/home/borg/scripts -v borgbackup-data-ssh:/home/borg/.ssh -v /path/to/data:/home/borg/data:ro sebcworks/borgbackup-client monthly
    -> OR:
    docker run --name borg-client-monthly -v borgbackup-scripts:/home/borg/scripts -v borgbackup-data-ssh:/home/borg/.ssh --volumes-from my-container:ro sebcworks/borgbackup-client monthly

### Launching the container(s)

Once that the containers are created, we can launch the backup process easily with a cron job for example.

Command is then:

    docker start -a borg-client-daily
    docker start -a borg-client-monthly

