#!/bin/bash

SERVER_IP=aaa.bbb.ccc.ddd
SERVER_PORT=2222
REPOSITORY_NAME=repository-name

# May be any other directory accessible to the user who run the script ("root" by default)
DATADIR=/home/borg/data

REPOS="borg@$SERVER_IP:$REPOSITORY_NAME"
BACKUP_DATE=`date "+%Y-%m-%d.%H:%M:%S"`
LOGFILE=$DATADIR/borgbackup_${BACKUP_DATE}.log

STATUS_FILE="/home/borg/.running_borg_backup"

OPTS="--one-file-system --exclude-caches -v --stats --compression zlib,4"

# Optionnaly encrypted passphrase,
# encryption done with command: echo -n "unencrypted passphrase" | openssl enc -aes-256-cbc -a -e -k $PASSPHRASE_ENC > $PASSPHRASE_FILE
PASSPHRASE_FILE="/home/borg/scripts/key.to.the.attic"
PASSPHRASE_ENC="WeNeverKnow"

# Get the encrypted passphrase and decrypt it
if [ ! -r "$PASSPHRASE_FILE" ]; then
    echo "FATAL ERROR: Can't read the passphrase file!"
    exit
fi

PASSPHRASE=`cat $PASSPHRASE_FILE | openssl enc -aes-256-cbc -a -d -k $PASSPHRASE_ENC`

# Skip if previous wasn't finished
if [ -f "$STATUS_FILE" ]; then
    echo "FATAL: Can't run Borg as it seems to be already running"
    exit
fi

echotime () {
  echo "[`date "+%Y-%m-%d %H:%M:%S"`]: $1"
}

# Log everything
exec >> $LOGFILE 2>&1




# Export the passphrase
export BORG_PASSPHRASE=$PASSPHRASE

# Export the ssh command to run
# Note: To avoid problems with "missing TTY", we have to add the option with the path to the unknown hosts file.
# With this configuration, we have to know the server, so we have to connect once with the volume of ssh data (for example, at repository init)
# But we have to "renew" this file if we re-create the server container or if we do anything that will change its keys.
export BORG_RSH="ssh -p $SERVER_PORT -i /home/borg/.ssh/id_rsa -o UserKnownHostsFile=/home/borg/.ssh/known_hosts"




echotime "Starting Borg backup script: $0"
touch "$STATUS_FILE"

# Do the backups

# Lazy process, do 1 archive per folder in $DATADIR
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for dir in `find $DATADIR -mindepth 1 -maxdepth 1 -type d`
do
    echotime "Doing $dir folder"
    borg create $OPTS $REPOS::$dir-$BACKUP_DATE $DATADIR/$dir
done

# Prune old archives
prune_opts="-v -s"
prune_keep_params="--keep-daily 3 --keep-weekly 3 --keep-monthly 3"

echotime "Starting pruning strategies..."
for dir in `find $DATADIR -mindepth 1 -maxdepth 1 -type d`
do
    borg prune $prune_opts $REPOS --prefix $dir $prune_keep_params
done

IFS=$SAVEIFS

rm "$STATUS_FILE"
echotime "Script ended."

